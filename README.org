#+TITLE:Actually simple dwm
#+AUTHOR:Strawberry-Milkshake500

* Dependencies
- Xlib header files
- [[https://github.com/uditkarode/libxft-bgra]] (for rendering emojis in the title bar or the status bar place)
- [[https://gitlab.com/Strawberry-milkshake500/JetBrainsMonoNerdFontCompleteBandit-Medium]] (The best font according to me)

* Patches
This build of dwm has only one patch which is the xrdb patch([[https://dwm.suckless.org/patches/xrdb/]]), and that is to instantly load and reload the pywal colorscheme colors
using the reload keybinding(keybindings are listed below)
* Code That I Have Borrowed:
 Fix transparent borders [[https://github.com/szatanjl/dwm/commit/1529909466206016f2101457bbf37c67195714c8]] C:


* Keybindings
Note: Super is the windows key on most keyboards
All of scripts are in my scripts repo: [[https://gitlab.com/Strawberry-milkshake500/dmscripts]]
| Binding                      | Command                                                     |
|------------------------------+-------------------------------------------------------------|
| Super + d                    | Launch Dmenu                                                |
| Super + Enter                | Launch St (Terminal)                                        |
| Super + x                    | Launch Newsboat rss reader                                  |
| Super + Shift + x            | Launch Qutebrowser                                          |
| Super + Shift + Control +  x | Launch Firefox                                              |
| Super + z                    | Launch Ncmpcpp music player (requires mpd)                  |
| Super + Shift + z            | Launch Passmenu (dmenu frontend for gnu pass)               |
| Super + a                    | Launch Htop system monitor                                  |
| Super + Shift + a            | Launch Emacs                                                |
| Super + c                    | Launch Dino (Xmpp client)                                   |
| Printscreen                  | Launch my Screenshot script (requires maim)                 |
| Shift + Printscreen          | Launch my Ffmpeg record script                              |
| ScrollLock                   | Launch my Emoji picker script                               |
| Super + e                    | Launch Pulsemixer                                           |
| Super + Shift + e            | Launch Claws mail client                                    |
| Super + .                    | Play the next song in mpd playlist                          |
| Super + ,                    | Play the previous song in the mpd playlist                  |
| Super + w                    | Pause the current playing song in the mpd playlist          |
| Super + p                    | Toggle random mode in mpd playlist                          |
| Pausebreak                   | Set a random wallpaper using my random set_wallpaper script |
| Super + F5                   | Reload the colorscheme via the Xresources file              |
| Super + Space                | Makes the higlighted window the master window               |
| Super + o                    | Increases the number of master windows                      |
| Super + Shift + o            | Decreases the number of master windows                      |
| Super + Shift + q            | Run the dmenu quit script (dmenusysact)                     |

* Installation instructions
- Install all the dependencies
- =git clone https://gitlab.com/Strawberry-milkshake500/very-simple-dwm.git=
- =cd very-simple-dwm=
- =sudo make clean install=
